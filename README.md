# CloudWalk test

## Requirements

* [Ruby](https://www.ruby-lang.org/en/documentation/installation/) (sample apps were written with Ruby 3.0.2)
* [Bundler for Ruby](http://bundler.io/), a dependency manager

## Setup

```bash
bundle
```

## Running the script

```bash
bundle exec ruby main.rb
```

## Running the tests

```bash
bundle exec rspec 
```
