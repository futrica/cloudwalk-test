require 'json'
require_relative 'lib/game_match_parser'

file_path = File.join(File.dirname(__FILE__), 'public/qgames.log')
file = File.new(file_path)

game_match_parser = GameMatchParser.new(file)
parsed_object = game_match_parser.match_kill_log

puts 'Matches information'
puts ''
puts JSON.pretty_generate(parsed_object)
