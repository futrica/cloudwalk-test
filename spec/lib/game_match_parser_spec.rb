# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/game_match_parser'

RSpec.describe GameMatchParser do
  let(:file_path) { File.join(File.dirname(__FILE__), '../../public/qgames.log') }
  let(:file) { File.new(file_path) }

  describe '.match_kill_log' do
    it 'returns an array of objects with information from the matches' do
      game_match_parser = GameMatchParser.new(file)

      parsed_object = game_match_parser.match_kill_log

      expect(parsed_object).to be_an(Array)
      expect(parsed_object[1]).to eq({"game_2"=>{"total_kills"=>11, "players"=>["Isgalamido", "Mocinha"], "kills"=>{"Isgalamido"=>-9, "Mocinha"=>0}}})
    end
  end

  describe '.end_match' do
    context 'when current line matchs with END_GAME_REGEX and previous_line is not empty and previous line do not match with END_GAME_REGEX' do
      it 'returns true' do
        line = '20:37 ------------------------------------------------------------'
        previous_line = '20:37 ShutdownGame:'
        game_match_parser = GameMatchParser.new(file)

        end_match = game_match_parser.end_match(line, previous_line)

        expect(end_match).to be_truthy
      end
    end

    context 'when current line matchs with END_GAME_REGEX and previous_line is empty' do
      it 'returns false' do
        line = '20:37 ------------------------------------------------------------'
        previous_line = ''
        game_match_parser = GameMatchParser.new(file)

        end_match = game_match_parser.end_match(line, previous_line)

        expect(end_match).to be_falsy
      end
    end

    context 'when current line matchs with END_GAME_REGEX and previous_line matchs with END_GAME_REGEX' do
      it 'returns false' do
        line = '20:37 ------------------------------------------------------------'
        previous_line = '20:37 ------------------------------------------------------------'
        game_match_parser = GameMatchParser.new(file)

        end_match = game_match_parser.end_match(line, previous_line)

        expect(end_match).to be_falsy
      end
    end
  end

  describe '.valid_kill_line?' do
    context 'when line matchs with KILL_LOG_REGEX' do
      it 'returns ' do
        game_match_parser = GameMatchParser.new(file)
        line = ' 20:54 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT'

        valid_kill_line = game_match_parser.valid_kill_line?(line)

        expect(valid_kill_line).to be_truthy
      end
    end

    context 'when line does not matchs with KILL_LOG_REGEX' do
      it 'returns ' do
        game_match_parser = GameMatchParser.new(file)
        line = ' 20:37 ClientBegin: 2'

        valid_kill_line = game_match_parser.valid_kill_line?(line)

        expect(valid_kill_line).to be_falsy
      end
    end
  end

  describe '.extracted_players' do
    it 'returns an array of players' do
      game_match_parser = GameMatchParser.new(file)
      line = ' 21:42 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT'

      players = game_match_parser.extracted_players(line)

      expect(players).to be_an(Array)
      expect(players).to eq(['<world>', 'Isgalamido'])
    end
  end
end
