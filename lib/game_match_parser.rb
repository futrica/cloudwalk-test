# frozen_string_literal: true

class GameMatchParser
  PLAYERS_REGEX = /.*\:\s(.+)\skilled\s(.+)\sby/
  WORLD_PLAYER = '<world>'
  KILL_LOG_REGEX = /Kill/
  END_GAME_REGEX = /------------------------------------------------------------/

  attr_reader :log_file
  attr_accessor :kills

  def initialize(log_file)
    @log_file = log_file
    @game_number = 0
    initialize_match
  end

  def match_kill_log
    parsed_lines = []
    previous_line = ''

    log_file.each do |line|
      parse_line(line) if valid_kill_line?(line)

      if end_match(line, previous_line)
        @game_number += 1
        parsed_lines << match_object
        initialize_match
      end

      previous_line = line
    end

    parsed_lines
  end

  def match_object
    {
      game => {
        'total_kills' => @total_kills,
        'players' => @players,
        'kills' => @kills
      }
    }
  end

  def end_match(line, previous_line)
    line.match(END_GAME_REGEX) && previous_line != '' && !previous_line.match(END_GAME_REGEX)
  end

  def valid_kill_line?(line)
    line.match(KILL_LOG_REGEX)
  end

  def extracted_players(line)
    line.match(PLAYERS_REGEX)[1, 2]
  end

  private

  def game
    "game_#{@game_number}"
  end

  def initialize_match
    @total_kills = 0
    @players = []
    @kills = {}
  end

  def parse_line(line)
    initialize_player(line)
    map_kill(line)
    count_kills
  end

  def count_kills
    @total_kills += 1
  end

  def initialize_player(line)
    extracted_players(line).each do |player|
      if player != WORLD_PLAYER
        add_player_to_players_list(player)
        initialize_player_kills(player)
      end
    end
  end

  def add_player_to_players_list(player)
    @players |= [player]
  end

  def initialize_player_kills(player)
    @kills[player] = 0 if @kills[player].nil?
  end

  def map_kill(line)
    killer, killed = extracted_players(line)

    add_kill(killer, killed)
    punish_player(killer, killed)
  end

  def add_kill(killer, killed)
    @kills[killer] += 1 if !killed_by_world(killer) && !suicide(killer, killed)
  end

  def killed_by_world(killer)
    killer == WORLD_PLAYER
  end

  def suicide(killer, killed)
    killer == killed
  end

  def punish_player(killer, killed)
    @kills[killer] -= 1 if suicide(killer, killed)
    @kills[killed] -= 1 if killed_by_world(killer)
  end
end
